import 'package:app/main.dart';
import 'package:app/models/transactions.dart';
import 'package:app/poviders/transaction_povider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FormScreen extends StatelessWidget {
  final formkey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final amountController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("แบบฟร์อมบันทึกข้อมูล"),
        ),
        body: Padding(
          padding: EdgeInsets.all(10.0),
          child: Form(
              key: formkey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormField(
                    decoration: new InputDecoration(labelText: "ชื่อรายการ"),
                    autofocus: false,
                    controller: titleController,
                    validator: (String str) {
                      if (str.isEmpty) {
                        return "กรุณาป้อนชื่อรายการ";
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    decoration: new InputDecoration(labelText: "จำนวนเงิน"),
                    keyboardType: TextInputType.number,
                    controller: amountController,
                    validator: (String str) {
                      if (str.isEmpty) {
                        return "กรุณาป้อนจำนวนเงิน";
                      }
                      if (double.parse(str) <= 0) {
                        return "กรุณาป้อนตัวเลขมากกว่า 0";
                      }
                      return null;
                    },
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(
                      child: Text("เพิ่มข้อมูล"),
                      color: Colors.green,
                      textColor: Colors.white,
                      onPressed: () {
                        if (formkey.currentState.validate()) {
                          var title = titleController.text;
                          var amount = amountController.text;

                          Transactions dataAdd = Transactions(
                              title: title,
                              amount: double.parse(amount),
                              date: DateTime.now());

                          var povider = Provider.of<TransactionPovider>(context,
                              listen: false);
                          povider.addTransaction(dataAdd);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  fullscreenDialog: true,
                                  builder: (context) {
                                    return MyHomePage();
                                  }));
                        }
                      })
                ],
              )),
        ));
  }
}
