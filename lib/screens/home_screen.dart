import 'package:app/models/transactions.dart';
import 'package:app/poviders/transaction_povider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'form_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void initState() {
    super.initState();
    Provider.of<TransactionPovider>(context, listen: false).initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("แอพบัญชี"),
          actions: [
            IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () {
                  SystemNavigator.pop();
                  // Navigator.push(context, MaterialPageRoute(builder: (context) {
                  //   return FormScreen();
                  // }));
                })
          ],
        ),
        body: Consumer(
            builder: (context, TransactionPovider povider, Widget clild) {
          var count = povider.transaction.length;
          if (count <= 0) {
            return Center(
                child: Text(
              "ไม่พบข้อมูล",
              style: TextStyle(fontSize: 40),
            ));
          } else {
            return ListView.builder(
                itemCount: povider.transaction.length,
                itemBuilder: (context, int index) {
                  Transactions data = povider.transaction[index];
                  return Card(
                    elevation: 10,
                    margin: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 30,
                        child: FittedBox(
                          child: Text(data.amount.toString()),
                        ),
                      ),
                      title: Text(data.title),
                      subtitle: Text(
                          DateFormat("dd/MM/yyyy HH:mm:ss").format(data.date)),
                    ),
                  );
                });
          }
        }));
  }
}
