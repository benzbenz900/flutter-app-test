import 'package:app/poviders/transaction_povider.dart';
import 'package:app/screens/form_screen.dart';
import 'package:app/screens/home_screen.dart';
import 'package:app/screens/qr_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) {
          return TransactionPovider();
        })
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
        home: MyHomePage(title: 'แอพบัญชี'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.blueGrey,
          body: TabBarView(
            children: [HomeScreen(), FormScreen()],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: startQR,
            child: Icon(Icons.qr_code),
          ),
          bottomNavigationBar: TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.list),
                text: "รายการ",
              ),
              Tab(
                icon: Icon(Icons.add),
                text: "เพิ่มข้อมูล",
              )
            ],
          ),
        ));
  }

  startQR() {
    Navigator.push(
        context,
        MaterialPageRoute(
            fullscreenDialog: true,
            builder: (context) {
              return QRViewExample();
            }));
  }
}
