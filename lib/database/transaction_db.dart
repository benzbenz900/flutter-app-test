import 'dart:io';

import 'package:app/models/transactions.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

class TransactionDB {
  String dbName;

  TransactionDB({this.dbName});

  Future<Database> openDB() async {
    Directory appDri = await getApplicationDocumentsDirectory();
    String dbLocation = join(appDri.path, dbName);
    DatabaseFactory dbFactory = databaseFactoryIo;
    Database db = await dbFactory.openDatabase(dbLocation);
    return db;
  }

  Future<int> insertData(Transactions data) async {
    var db = await this.openDB();
    var store = intMapStoreFactory.store("expense");

    var keyID = store.add(db, {
      "title": data.title,
      "amount": data.amount,
      "date": data.date.toIso8601String()
    });
    db.close();
    return keyID;
  }

  Future<List<Transactions>> loadAllData() async {
    var db = await this.openDB();
    var store = intMapStoreFactory.store("expense");

    var snap = await store.find(db,
        finder: Finder(sortOrders: [SortOrder(Field.key, false)]));
    // ignore: deprecated_member_use
    List transactionList = List<Transactions>();
    for (var rec in snap) {
      transactionList.add(Transactions(
          title: rec["title"],
          amount: rec["amount"],
          date: DateTime.parse(rec["date"])));
    }
    return transactionList;
  }
}
