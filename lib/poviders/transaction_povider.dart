import 'package:app/database/transaction_db.dart';
import 'package:app/models/transactions.dart';
import 'package:flutter/foundation.dart';

class TransactionPovider with ChangeNotifier {
  List<Transactions> transaction = [];

  void initData() async {
    var db = TransactionDB(dbName: "transactions.db");
    transaction = await db.loadAllData();
    notifyListeners();
  }

  void addTransaction(Transactions data) async {
    var db = TransactionDB(dbName: "transactions.db");

    await db.insertData(data);
    transaction = await db.loadAllData();
    
    notifyListeners();
  }
}
